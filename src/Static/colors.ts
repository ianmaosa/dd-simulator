const COLORS = {
  WHITE: "#FFF",
  BLACK: "#000",
  GRAY: "#d3d3d38f",
  LIGHTGREEN: "#00800073",
  LIGHTBLUE: "#add8e6a1",
  BLUE: "#0078d496",
  GOLD: "#ffd7005e",
  ORANGE: "#ffa50082",
  RED: "#ff0000ad",
  YELLOW: "#ffd700a3",
  GREE: "#00b700a3",
  PINK: "#c72ac770",
};

export default COLORS;
