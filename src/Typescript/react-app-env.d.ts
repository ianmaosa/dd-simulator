/// <reference types="react-scripts" />

interface Data {
  [drug: string]: Drug;
}

interface Drug {
  key: string;
  text: string;
  buy: number;
  sell: number;
  bought: number;
  sold: number;
  stock: number;
  color: string;
  tag: string[];
}
