import styled from "styled-components";
import { PADDING } from "../../Static/sizes";
import COLORS from "../../Static/colors";

type StyleTypes = {
  color?: string;
};

const Container = styled.table`
  width: 100%;
  padding: ${PADDING[1]}rem;
`;

const Header = styled.thead`
  background: ${COLORS.GRAY};
`;

const Body = styled.tbody``;

const Footer = styled.tfoot``;

const Row = styled.tr``;

const HeaderCell = styled.th``;

const Cell = styled.td<StyleTypes>`
  text-align: right;
  background-color: ${(props) => props.color || "inherit"};
  box-shadow: 0 0 3px -2px black;
`;

export { Container, Header, Body, Footer, Row, HeaderCell, Cell };
