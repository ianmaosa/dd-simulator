import React, { useState, useEffect } from "react";
import { Wrapper } from "./styled.components";
import { Subtitle } from "../Title";
import { Container, CardContainer } from "./styled.components";
import Card from "../Card";
import Controls from "../Controls";

type PropTypes = {
  drugs: Drug[];
  setDrugs: React.Dispatch<React.SetStateAction<Drug[]>>;
};

const printCards = (
  drugs: Drug[],
  selected: Drug | undefined,
  setSelected: React.Dispatch<React.SetStateAction<Drug | undefined>>
) =>
  drugs.map((drug, index) => (
    <Card
      handleClick={setSelected}
      key={index}
      drug={drug}
      selected={selected}
    />
  ));

const Setup = ({ drugs, setDrugs }: PropTypes) => {
  const [selected, setSelected] = useState<Drug | undefined>(undefined);

  useEffect(() => {}, [drugs, selected]);

  return (
    <Wrapper>
      <Subtitle>Setup</Subtitle>
      <Container>
        <CardContainer>
          {printCards(drugs, selected, setSelected)}
        </CardContainer>
        <Controls
          setDrugs={setDrugs}
          drugs={drugs}
          selected={selected}
          setSelected={setSelected}
        />
      </Container>
    </Wrapper>
  );
};

export default React.memo(Setup);
