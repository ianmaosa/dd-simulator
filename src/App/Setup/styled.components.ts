import styled from "styled-components";

const Wrapper = styled.section`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
`;

const Container = styled.div`
  display: flex;
  flex-grow: 1;
`;

const CardContainer = styled.div`
  flex-grow: 1;
  display: grid;
  grid-template-columns: repeat(3, minmax(150px, 1fr));
  grid-column-gap: 10px;
  grid-row-gap: 10px;
`;

const ControlContainer = styled.div`
  min-width: 300px;
  margin-left: 1rem;
`;

export { Wrapper, CardContainer, Container, ControlContainer };
