import React from "react";
import { Wrapper } from "./styled-components";

import { TextField } from "office-ui-fabric-react/lib/TextField";
import { PrimaryButton, Stack, DefaultButton } from "office-ui-fabric-react";

import data from "../../Static/data.json";

type PropTypes = {
  selected: Drug | undefined;
  setSelected: React.Dispatch<React.SetStateAction<Drug | undefined>>;
  drugs: Drug[];
  setDrugs: React.Dispatch<React.SetStateAction<Drug[]>>;
};

const save = (
  selected: Drug | undefined,
  setSelected: React.Dispatch<React.SetStateAction<Drug | undefined>>,
  drugs: Drug[],
  setDrugs: React.Dispatch<React.SetStateAction<Drug[]>>
) => {
  if (selected) {
    const mapped = drugs.map((drug) => {
      if (drug.key === selected.key) {
        return selected;
      }
      return drug;
    });
    setSelected(undefined);
    setDrugs(mapped);
  }
};

const handleChange = (
  key: string,
  setSelected: React.Dispatch<React.SetStateAction<Drug | undefined>>,
  value?: string,
  selected?: Drug
) => {
  if (value && selected) {
    const newObj = {
      ...selected,
      [key]: parseInt(value),
    };
    setSelected(newObj);
  }
};

const reset = (setDrugs: React.Dispatch<React.SetStateAction<Drug[]>>) =>
  setDrugs(Object.values(data));

const Controls = ({ drugs, setDrugs, selected, setSelected }: PropTypes) => (
  <Wrapper>
    <Stack tokens={{ childrenGap: 10 }}>
      <TextField
        label="Stock (g)"
        value={selected ? selected.stock.toString() : ""}
        onChange={(e, value) =>
          handleChange("stock", setSelected, value, selected)
        }
      />
      <TextField
        label="Buy ($)"
        value={selected ? selected.buy.toString() : ""}
        onChange={(e, value) =>
          handleChange("buy", setSelected, value, selected)
        }
      />
      <TextField
        label="Sell ($)"
        value={selected ? selected.sell.toString() : ""}
        onChange={(e, value) =>
          handleChange("sell", setSelected, value, selected)
        }
      />
      <PrimaryButton
        text="Save"
        onClick={() => save(selected, setSelected, drugs, setDrugs)}
        disabled={!selected}
      />
      <DefaultButton text="Reset" onClick={() => reset(setDrugs)} />
    </Stack>
  </Wrapper>
);

export default Controls;
