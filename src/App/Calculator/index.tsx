import React from "react";
import { Wrapper } from "./styled.components";
import { Subtitle } from "../Title";
import Input from "../Input";
import Table from "../Table";

type PropTypes = {
  drugs: Drug[];
  setDrugs: React.Dispatch<React.SetStateAction<Drug[]>>;
};

const Calculator = ({ drugs, setDrugs }: PropTypes) => (
  <Wrapper>
    <Subtitle>Calculator</Subtitle>
    <Input drugs={drugs} setDrugs={setDrugs} />
    <Table drugs={drugs} />
  </Wrapper>
);

export default React.memo(Calculator);
