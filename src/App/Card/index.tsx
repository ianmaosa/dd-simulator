import React from "react";
import { Wrapper, Info } from "./styled-components";

type PropTypes = {
  drug: Drug;
  handleClick: Function;
  selected: Drug | undefined;
};

const Card = ({ drug, handleClick, selected }: PropTypes) => (
  <Wrapper
    onClick={() =>
      handleClick(selected && selected.key === drug.key ? undefined : drug)
    }
    selected={!!(selected && selected.key === drug.key)}
  >
    <h4>{drug.text}</h4>
    <Info>
      <h6>Stock: {drug.stock}g</h6>
      <h6>Buy: {drug.buy}$</h6>
      <h6>Sell: {drug.sell}$</h6>
    </Info>
    <h6>Tags: {drug.tag.toString()}</h6>
  </Wrapper>
);

export default Card;
