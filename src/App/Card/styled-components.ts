import styled from "styled-components";
import COLORS from "../../Static/colors";

type StyleTypes = {
  selected: boolean;
};

const Wrapper = styled.article<StyleTypes>`
  padding: 10px;
  background: ${(props) => (props.selected ? COLORS.GRAY : "inherit")};
  box-shadow: 0 0 4px 0 black;
  cursor: pointer;
  transition: all 0.5s ease;
  overflow-wrap: break-word;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  &:hover {
    transform: scale(1.02);
    transition: transform 0.5s ease;
  }

  &:active {
    transform: scale(1);
    transition: transform 0.5s ease;
  }
`;

const Info = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
  justify-content: space-evenly;

  h6 {
    margin-bottom: 0;
    margin-top: 0;
  }
`;

export { Wrapper, Info };
