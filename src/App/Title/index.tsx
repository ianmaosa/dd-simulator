import React from "react";
import { Header1, Header2 } from "./styled-components";

type PropTypes = {
  children: string;
};

const Title = ({ children }: PropTypes) => <Header1>{children}</Header1>;

export const Subtitle = ({ children }: PropTypes) => (
  <Header2>{children}</Header2>
);

export default Title;
