import React from "react";
import { Container } from "./styled-components";
import Title from "../Title";

const Header = () => (
  <Container>
    <Title>Drug Dealer Simulator Calculator</Title>
    <p>Calculator for the video game "Drug Dealer Simulator"</p>
  </Container>
);

export default Header;
