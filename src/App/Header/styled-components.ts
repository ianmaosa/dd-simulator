import styled from "styled-components";

const Container = styled.header`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  p {
    margin: 0;
  }
`;

export { Container };
