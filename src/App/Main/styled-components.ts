import styled from "styled-components";
import { MARGIN } from "../../Static/sizes";

const Container = styled.main`
  flex-grow: 1;
  margin: 1rem ${MARGIN[5]}rem;
  display: flex;
  flex-direction: column;
`;

export { Container };
