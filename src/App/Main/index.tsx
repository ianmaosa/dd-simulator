import React, { useState, useEffect } from "react";
import { Container } from "./styled-components";
import Setup from "../Setup";
import Calculator from "../Calculator";

type PropTypes = {
  data: Drug[];
};

const Main = ({ data }: PropTypes) => {
  const [drugs, setDrugs] = useState<Drug[]>(data);

  useEffect(() => {
    localStorage.setItem("data", JSON.stringify(drugs));
  }, [drugs]);

  return (
    <Container>
      <Calculator drugs={drugs} setDrugs={setDrugs} />
      <Setup drugs={drugs} setDrugs={setDrugs} />
    </Container>
  );
};

export default React.memo(Main);
