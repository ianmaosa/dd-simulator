import React from "react";
import Wrapper from "./styled-components";
import Header from "./Header";
import Main from "./Main";

type PropTypes = {
  data: Drug[];
};

const App = ({ data }: PropTypes) => (
  <Wrapper>
    <Header />
    <Main data={data} />
  </Wrapper>
);

export default App;
